# DotA Twitch Terminal Player

A simple python script which scrapes data on current twitch streams of the popular video game "DotA", displays them in order of popularity, while showing the stream name, view count and stream title. The user is then allowed to select a stream to watch and the url is piped into streamlink.

![Twitch Player Demo](demo/demo.gif)
