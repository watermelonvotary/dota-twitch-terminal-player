import requests
import re
import os
import sys
from pathlib import Path
from bs4 import BeautifulSoup

myfile=Path('quality.txt')
if myfile.is_file():
    quality = open(myfile ,'r').read()
else:
    quality = input('What quality would you like to stream at? (worst or best):').replace(' ','')
    open(myfile, 'w').write(quality)
    quality = open(myfile, 'r').read()

#INITIAL VARIABLE SETUPS
stream_data=[] #parsed as [streamer name, view count, broadcast title]
headers = {'User-Agent':'Mozilla/5.0'}
page = requests.get('https://www.liquiddota.com/stream/')
soup = BeautifulSoup(page.content, 'html.parser')
spaced = '-'*50

#print('\x1b[1;31m'+ spaced  + 'FEATURED STREAMS' + spaced + '\x1b[0m')


featured_stream = soup.findAll('a', attrs={'class': 'featurebox'})
for a in featured_stream:
    info = a.text.strip()
    info = info.split('\n')
    info[0].replace(' ','')
    stream_data.append([info[1], info[0], info[2]])


stream_info = soup.findAll('tr', attrs={'class':['odd', 'even']})
for a in stream_info:
    info = a.text.strip()
    info = info.split('\n')
    info[0].replace(' ','')
    info[1] = (info[1][:70] + '..') if len(info[1]) > 70 else info[1]
    stream_data.append([info[0], info[2], info[1]])
    

def Printer():
    for x in range(len(stream_data)):
        q= (len(stream_data)-x)-1
        print("%-15s %-25s %-15s %-15s"%('\x1b[1;31m' + str(q) +  '\x1b[0m', stream_data[q][0], stream_data[q][1], stream_data[q][2]))
        x+=1

Printer()

user_response = input('Enter the number of the stream, or the streamer\'s name , to begin watching:')
user_response.replace(" ","")
user_interpreter = stream_data[int(user_response)][0]

try:
    command = 'streamlink http://www.twitch.tv/' + user_interpreter + ' ' + quality + ' & exit'
except ValueError:
    command = 'streamlink http://www.twitch.tv/' + user_response + ' ' + quality + ' & exit'

os.system(command)

